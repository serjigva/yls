#define _XOPEN_SOURCE
#define _DEFAULT_SOURCE

#include <dirent.h>
#include <fcntl.h>
#include <grp.h>
#include <errno.h>
#include <libgen.h>
#include <limits.h>
#include <pwd.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/queue.h>
#include <time.h>
#include <unistd.h>

#include "yls.h"

int get_dirlist(char *dir, struct slisthead *head)
{
    int n = 0;
    struct entry *dn = NULL;
    DIR *dirfd = NULL;
    if (NULL == (dirfd = opendir(dir))) {
        perror(dir);
        exit(EXIT_FAILURE);
    };

    struct dirent *ent;
    while (NULL != (ent = readdir(dirfd))) {
        if (ent->d_name[0] == '.')
            continue;
        dn = malloc(sizeof(struct entry));
        SLIST_INSERT_HEAD(head, dn, entries);
        strncpy(dn->data, ent->d_name, strlen(ent->d_name) + 1);
        n++;
    }
    closedir(dirfd);
    return n;
}

void perms_convert(mode_t val, char *mode)
{
    int i, j;
    *mode = (val & S_IFMT) == S_IFDIR ? 'd' : '-';
    for (i = 1; i <= 9; i+=3)
        for (j = 0; j < 3; j++)
            mode[i+j] = val & perms[i+j-1] ? md[j] : '-';
}

void get_mtime(__time_t *st_m, char *mtime)
{
    strftime(mtime, 16, "%b %e %H:%M", localtime(st_m));
}

File_ent
file_stat(char *full_path, int path_n, char *file_name)
{
    struct stat sb;
    File_ent rfn;
    memset(&rfn, 0, sizeof(File_ent));

    char local_path[path_n + strlen(file_name) + 1];
    sprintf(local_path, "%s/%s", full_path, file_name);

    stat(local_path, (struct stat *) &sb);

    perms_convert(sb.st_mode, rfn.mode);
    rfn.user      = (char *) getpwuid(sb.st_uid)->pw_name;
    rfn.group     = (char *) getgrgid(sb.st_gid)->gr_name;
    rfn.size      = (intmax_t) sb.st_size;
    get_mtime(&sb.st_mtime, rfn.mtime);
    rfn.filename  = file_name;

    return rfn;
}

void files_collect(char *path, struct slisthead *head, File_ent *file_entity)
{
    int i = 0, dir_n = strlen(path) + 1;
    struct entry *np;
    SLIST_FOREACH(np, head, entries)
        file_entity[i++] = file_stat(path, dir_n, np->data);
}

int filename_cmp(const void *s, const void *d)
{
    const File_ent *a = (const File_ent *) s;
    const File_ent *b = (const File_ent *) d;
    return strcmp(((File_ent *)a)->filename, ((File_ent *)b)->filename);
}

void print_files(File_ent *files, const int n, const int l)
{
    int i = 0;
    if (l)
        for (; i < n; i++) {
            File_ent pf = files[i];
            printf("%s %s %s %10jd %s %s\n",
                pf.mode,
                pf.user,
                pf.group,
                pf.size,
                pf.mtime,
                pf.filename);
        }
    else {
        for (; i < n; i++)
            printf("%s  ", (files+i)->filename);
        printf("\n");
    }
}

int main(int argc, char **argv)
{
    extern int optind;
    int lf = 0, opt;
    while (-1 != (opt = getopt(argc, argv, "l"))) {
        switch (opt) {
            case 'l':
                lf = 1;
                break;
            default:
                printf("Usage: %s [-l] name\n", argv[0]);
                return EXIT_FAILURE;
        }
    }

    char *path = NULL;
    if (optind >= argc)
        path = strndup(".\0", 2);
    else {
        int pl = 1 + strlen(argv[optind]);
        path = strndup(argv[optind], pl);
    }

    int fd;
    if (-1 == (fd = open(path, O_RDONLY))) {
        perror(path);
        return EXIT_FAILURE;
    }

    struct stat sb;
    if (-1 == fstat(fd, &sb)) {
        printf("Could not gather fstat of %s\n", path);
        return EXIT_FAILURE;
    }
    if ((sb.st_mode & S_IFMT) == S_IFDIR) {
        close(fd);

        struct entry *n1;
        struct slisthead head;
        /* Linked list magic */
        SLIST_INIT(&head);

        int fn = get_dirlist(path, &head);

        File_ent *fst = NULL;
        if (NULL == (fst = (File_ent *) malloc(sizeof(File_ent) * fn))) {
            printf("Memory could not be allocated..\n");
            return EXIT_FAILURE;
        }

        /* Collect meta for each file inside directory */
        files_collect(path, &head, fst);

        /* Sort by name */
        qsort(fst, fn, sizeof(File_ent), filename_cmp);

        /* Print result and free() data */
        print_files(fst, fn, lf);
        free(fst);

        /* List Deletion. */
        while (!SLIST_EMPTY(&head)) {
            n1 = SLIST_FIRST(&head);
            SLIST_REMOVE_HEAD(&head, entries);
            free(n1);
        }

    }
    else {
        File_ent e;
        char *full_path = dirname(path);
        int fp_n = strlen(full_path);
        char *filename = basename(path);
        e = file_stat(full_path, fp_n, filename);
        print_files(&e, 1, lf);
        close(fd);
    }

    free(path);
    return EXIT_SUCCESS;
}
