# What is it
This is Yet another LS.

Restricted version of **ls** tool from GNU/coreutils supports **-l** option and
relative pathes. Info about file available for both directories and files.

It uses POSIX API, singly linked list and alloc heap mem to store filedata.

# How to build and run
Run `make` or `make all` only, to build.
To run application, `make run` or `./yls -l` to show home dir or current dir
content accordingly.

# Technical details
App opens current directory or referred at command arguments, and collect
filedata to the heap memory.

**l** option appends on output some data, as file type, permissions, ownership
user and group, filesize, last modification time, and filename through
whitespaces.

Codestyle ANSI C.
