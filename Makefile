all: yls

yls: yls.c
	gcc -Wall -fPIC -ansi -o yls yls.c

run: yls
	./yls -l ~

clean:
	-rm yls 2>/dev/null
