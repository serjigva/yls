/* Certain structure to store filedata */

typedef struct {
    char mode[11];
    char *user;
    char *group;
    uintmax_t size;
    char mtime[16];
    char *filename;
} File_ent;

struct entry {
    char data[256];
    SLIST_ENTRY(entry) entries;
};

const char md[]   = {'r', 'w', 'x'};
const int perms[] = {S_IRUSR, S_IWUSR, S_IXUSR,
                     S_IRGRP, S_IWGRP, S_IXGRP,
                     S_IROTH, S_IWOTH, S_IXOTH};

SLIST_HEAD(slisthead, entry);

int get_dirlist(char *, struct slisthead *);

/* Gather stat of file through absolute path */
File_ent file_stat(char *, int, char *);

/* Run on directory and file_stat() of each file */
void files_collect(char *, struct slisthead *, File_ent *);

/* Comparator for sorting the files by name */
int filename_cmp(const void *, const void *);

void print_files(File_ent *, const int, const int);

void perms_convert(mode_t , char *);

void get_mtime(__time_t *, char *);
